// Invite for Bot -  https://discordapp.com/oauth2/authorize?client_id=630151012706942988&scope=bot&permissions=2146958847
const { AkairoClient } = require('discord-akairo');
// todo: fix this shit (module exports) [label: beta tests]
const msg = require('./utils/functions/Messages.js');
module.exports.msg = msg;
const config = require('./configs/config.json');
module.exports.config = config;
const { Databases, Guilds, Accounts, LR_Tables } = require('./storage/db.js');
module.exports.Databases = Databases;
module.exports.Guilds = Guilds;
module.exports.Accounts = Accounts;
module.exports.LR_Tables = LR_Tables;
const DataTables = require('./utils/functions/DataTables.js');
module.exports.DataTables = DataTables;
const Steamid = require('./utils/functions/Steamid.js');
module.exports.Steamid = Steamid;
const Workplace = require('./utils/functions/Workplace');
module.exports.Workplace = Workplace;
const Ranks = require('./utils/functions/Ranks');
module.exports.Ranks = Ranks;

// ? do something like this:
/* module.exports = {
    msg, config,
    Databases, Guilds,
    DataTables, Accounts,
    Steamid, Workplace
} */

const client = new AkairoClient({
    ownerID: config.owners,
    prefix: config.prefix,
    emitters: {
        process
    },
    // allowMention: true, // disable mention a bot, because this is broke bot logic get prefix data (prefix is empty)
    handleEdits: true,
    commandUtil: true,
    commandUtilLifetime: 600000,
    commandDirectory: './commands/',
    inhibitorDirectory: './inhibitors/',
    listenerDirectory: './listeners/',
    // automateCategories: true,
    defaultPrompt: {
        timeout: 'Срок истек, команда была отменена.',
        ended: 'Слишком много попыток, команда была отменена.',
        cancel: 'Команда была отменена.'
    },
}, {
    disableEveryone: true
});

client.login(config.token);