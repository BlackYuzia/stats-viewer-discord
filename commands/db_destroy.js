const { Command } = require('discord-akairo');
const { Workplace, msg, config } = require('../index.js');

class DestroyDatabase extends Command {
    constructor() {
        super('DestroyDatabase', {
            aliases: ['rem_database', "rem_db", "rdb"],
            channelRestriction: "guild",
            category: "database",
            description: 'Позволяет удалить базу данных от FPS',
            options: {
                usage: `rem_db`,
                example: `rem_db`,
                flags: []
            },
            split: 'quoted',
            args: [
                // todo: fix exploit or do better
                /* 
                {
                    id: 'gid',
                    type: "guild",
                    default: message => message.guild.id
                } */
            ]
        });
    }

    userPermissions(message) {
        const check = message.member.roles.cache.find(role => role.name === config.Permissions.role.name);
        // todo: throw this to .catch(err)
        if (!check) msg.embed.send(message, "ОШИБКА", `У вас нет роли с именем \`${config.Permissions.role.name}\`\nСоздайте и/или выдайте ее себе`, msg.colors.err);
        return check;
    }

    exec(message, args) {
        // todo: refactory in next versions 
        args["gid"] = message.guild.id;
        args["type"] = message.util.prefix.slice(0, -1).toUpperCase();

        return Workplace.destroyDatabaseSetup(args)
            .then((database) => {
                if (database) {
                    const title = `База для [\`${args["type"]}\`] статистики успешно удалена`;
                    return msg.embed.send(message, title, null, msg.colors.send);
                }
                // todo: throw to .catch(err)
                return msg.embed.send(message, "ОШИБКА", "Не могу найти базу для удаления (может ее и нет?)", msg.colors.err);
            }).catch((err) => {
                console.error(err);
                msg.embed.send(message, "ОШИБКА", "Что-то не так...", msg.colors.err);
                console.log("err", err);
                return err;
            });
    }
}

module.exports = DestroyDatabase;