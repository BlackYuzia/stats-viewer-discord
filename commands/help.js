const CommonUtil = require('../utils/CommandUtil');
const { MessageEmbed } = require('discord.js');
const { Command } = require('discord-akairo');
const { msg } = require('../index');

class HelpCommand extends Command {
    constructor() {
        super('help', {
            aliases: ['help', 'h'],
            category: "misc",
            description: 'Позволяет получить список модулей или команд, или детальную информацию про определенную команду.',
            options: {
                usage: `h [команда или категория]`,
                example: `h help`,
            },
            args: [
                {
                    id: 'key',
                    type: 'string',
                    match: 'content',
                    default: null
                },
            ],
        });
    }

    // Returns a list of all command categories, and also a list of commands for each category
    _getFullList(message, handler) {
        const embed = new MessageEmbed();
        handler.categories.forEach((v, k) => {
            const field = {
                name: k.toTitleCaseAll(),
                value: '',
                inline: true,
            };
            v.forEach((v2) => {
                const p = v2.prefix || handler.prefix(message);
                field.value += `\`${CommonUtil.emoji.enabled[v2.enabled]} ${p}${v2.aliases.join(`, ${p}`)}\`\n`;
            });
            field.value = `${field.value}`;
            embed.fields.push(field);
        });
        embed.color = msg.colors.help;
        return embed;
    }

    // Returns an embed listing all the command (and aliases) available for a given category
    _getCmdList(message, handler, cat) {
        const embed = new MessageEmbed();
        cat.forEach((v, k) => {
            const p = v.prefix || handler.prefix(message);
            const field = {
                name: `${p}${k}`,
                value: `\`${CommonUtil.emoji.enabled[v.enabled]} ${p}${v.aliases.join(`, ${p}`)}\`\n`,
                inline: true,
            };
            v.args.forEach((v2) => {
                field.value += `*${v2.type}* ${v2.id} (${v2.default()})\n`;
            });
            field.value = `${field.value}`;
            embed.fields.push(field);
        });
        embed.color = msg.colors.help;
        return embed;
    }

    _getCmdInfo(message, handler, cmd) {
        const embed = new MessageEmbed();
        const data = [];
        const p = cmd.prefix || handler.prefix(message);
        embed.title = `\`${p}${cmd.aliases.join(` / ${p}`)}\``;
        data.push(`**${cmd.description}**`);
        if (cmd.options.usage) data.push(`\`Шаблон: ${p}${cmd.options.usage}\``);
        if (cmd.options.example) data.push(`\`Пример: ${p}${cmd.options.example}\``);
        if (cmd.options.flags) {
            cmd.options.flags.find(f => { data.push(f); })
        };
        embed.description = data.join("\n");
        cmd.args.forEach((v) => {
            embed.fields.push({
                name: v.id,
                value: `Type: *${v.type}*\nDefault: ${v.default()}`,
                inline: true,
            });
        });
        embed.color = msg.colors.help;
        return embed;
    }

    exec(message, args) {
        const handler = this.handler;

        if (args.key) {
            const key = args.key
            const findCommand = handler.findCommand(args.key);
            const findCategory = handler.findCategory(args.key);

            if (findCategory) {
                const cat = findCategory;
                return message.util.send(`Список всех команд в данной категории: **${key.toTitleCaseAll()}**`,
                    { embed: this._getCmdList(message, handler, cat) });
            }
            else if (findCommand) {
                const cmd = findCommand;
                return message.util.send(`Информация по команде: **${key}**`,
                    { embed: this._getCmdInfo(message, handler, cmd) });
            }
            else {
                return message.util.send(`Не могу найти ни одной команды или категории с именем: **${key}**`);
            }
        }

        return message.util.send('Список всех категорий:',
            { embed: this._getFullList(message, handler) });
    }
}

module.exports = HelpCommand;