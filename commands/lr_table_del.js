const { Command } = require('discord-akairo');
const { Workplace, msg, config } = require('../index.js');

class LR_Table_Del extends Command {
    constructor() {
        super('LR_Table_Del', {
            aliases: ['del_table', "deltable", "rem_table", "remtable"],
            channelRestriction: "guild",
            prefix: ["lr_"],
            category: "lr tables",
            description: 'Позволяет удалить привязаную таблицу к LR статистике.',
            options: {
                usage: `del_table <table_name>`,
                example: `del_table lvl_base`,
                flags: []
            },
            split: 'quoted',
            args: [
                {
                    id: 'table_name',
                    prompt: {
                        start: () => {
                            return msg.embed.reply('**Введите префикс таблицы с сервером для удаления.**', this.options.example);
                        }
                    }
                }
            ]
        });
    }

    userPermissions(message) {
        const check = message.member.roles.cache.find(role => role.name === config.Permissions.role.name);
        // todo: throw this to .catch(err)
        if (!check) msg.embed.send(message, "ОШИБКА", `У вас нет роли с именем \`${config.Permissions.role.name}\`\nСоздайте и/или выдайте ее себе`, msg.colors.err);
        return check;
    }

    exec(message, args) {
        // todo: refactory in next versions 
        args["gid"] = message.guild.id;
        args["type"] = message.util.prefix.slice(0, -1).toUpperCase();

        return Workplace.delLRTable(args)
            .then((data) => {
                if (!data) throw "Не могу найти таблицу";

                const title = `Успешно удалена таблица \`${args["table_name"]}\`base`;
                return msg.embed.send(message, title, null, msg.colors.send);
            }).catch((err) => {
                console.log(this.id, err);
                // todo: добавить доп. инфу в футер .
                msg.embed.send(message, "ОШИБКА", err, msg.colors.err);
                return err;
            });


    }
}

module.exports = LR_Table_Del;