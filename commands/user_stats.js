const { Command } = require('discord-akairo');
const { Workplace, Steamid, Ranks, msg } = require('../index');

class CheckUserStats extends Command {
    constructor() {
        super('CheckUserStats', {
            aliases: ['stats', "st", "check", "find", "ch"],
            channelRestriction: "guild",
            category: "statistic",
            description: 'Позволяет посмотреть статистику игрока',
            options: {
                usage: `stats ["STEAMID2" or "STEAMID64" or "STEAMID3"]`,
                example: `stats STEAM_0:123456789 | stats 1234567890`,
                flags: []
            },
            split: 'quoted',
            args: [
                {
                    id: 'target',
                    default: null
                }
            ]
        });
    }

    async exec(message, args) {
        // todo: refactory in next versions 
        args["gid"] = message.guild.id;
        args["discord_id"] = message.author.id;
        args["type"] = message.util.prefix.slice(0, -1).toUpperCase();

        // todo: fix bug with throw err
        const attachError = [
            "У вас не привязан STEAMID к аккаунту и вы не указали данные игрока.",
            "**Привяжите steamid** или **используйте команду с аргументами**."
        ];

        if (args["type"] === "FPS") {

            if (args["target"]) args["target"] = Steamid.parseAccountID(args["target"]);
            else {
                // todo: Оптимизировать в следующий версиях
                const attached = await Workplace.findAccount(args)
                if (!args["target"] && !attached) throw msg.embed.send(message, "ОШИБКА", attachError, msg.colors.err);
                args["target"] = Steamid.parseAccountID(attached.steam_id);
            };

            // Get account_id, nickname, steam_id(64), ip
            const userData = Workplace.findUserByAccountID(args);
            const ServersList = Workplace.getFPSServersList(args);
            const userStats = Workplace.findUserStatsByAccountID(args);

            return Promise.all([userData, ServersList, userStats])
                .then((arr) => {
                    // todo: refactory some code
                    const data = {
                        userData: arr[0],
                        ServersList: arr[1],
                        userStats: arr[2]
                    };

                    if (!data.userData) throw "Не могу найти ни одного игрока."
                    data.userNickname = `**${data.userData.nickname}**`;
                    data.userSteamID2 = `**SteamID: ${Steamid.parseSteamID2(data.userData.steam_id)}**`;

                    if (data.userStats < 1) {
                        const body = [];
                        body.push(data.userSteamID2);
                        body.push("```Не могу найти никакой статистики этого игрока.```");

                        return msg.embed.send(message, data.userNickname, body, msg.colors.err)
                    };

                    const fieldsNames = [
                        "Очки",
                        // "Ранг", // ? Removed
                        "Убийств",
                        "Смертей",
                        "Помощь",
                        "Макс. Убийств за Раунд",
                        "Раундов Выиграно",
                        "Раундов Проиграно",
                        "Наиграно",
                        "Последнее Подключение",
                        /*         
                            Keys list:
                                            "points",
                                            "rank",
                                            "kills",
                                            "deaths",
                                            "assists",
                                            "round_max_kill",
                                            "round_win",
                                            "round_lose",
                                            "playtime",
                                            "lastconnect", */
                    ];

                    const keys = Object.keys(data.userStats[0].dataValues);

                    // Remove id, account_id, server_id keys
                    keys.splice(0, 3);
                    // Remove rank key;
                    keys.splice(1, 1);

                    data.userStats
                        .forEach(stat => {
                            const server = data.ServersList.find(server => server.id === stat.server_id);
                            const body = [];
                            const fieldsBodies = [];

                            // Rank getter Block 
                            data.rankElement = --stat.rank;
                            data.settingID = server.settings_rank_id;

                            const object = {
                                gid: args["gid"],
                                type: args["type"],
                                settingID: data.settingID,
                                rankElement: data.rankElement
                            }

                            const attachmentObj = Ranks.FPS_getRank(object);

                            body.push(`**Nickname:** ${data.userNickname}`);
                            body.push(data.userSteamID2);

                            keys.forEach(key => {

                                if (key === "lastconnect") {
                                    return fieldsBodies.push(parseTime(stat[key]));
                                }

                                if (key === "playtime") {
                                    return fieldsBodies.push(parseDate(stat[key]));
                                }

                                return fieldsBodies.push(stat[key]);
                            })

                            // add K/D stats
                            fieldsNames.splice(3, 0, "У/С");
                            fieldsBodies.splice(3, 0, (stat.kills / stat.deaths).toFixed(2));

                            const fields = msg.fn.createFields(fieldsNames, fieldsBodies, true);

                            attachmentObj
                                .then((attachmentObj) => {
                                    msg.embed.send(message, server.server_name, body, msg.colors.send, null, attachmentObj, fields);
                                })
                        });

                }).catch((err) => {
                    console.log("err", err);
                    msg.embed.send(message, "ОШИБКА", err, msg.colors.err, "Проверьте, разрешили ли вы доступ к базе с ip: 195.201.148.144");
                    return err;
                });
        }

        // if (args["type"] === "LR")        

        if (args["target"]) args["target"] = Steamid.parseSteamID2(args["target"]);
        else {
            // todo: Оптимизировать в следующий версиях
            const attached = await Workplace.findAccount(args);
            if (!args["target"] && !attached) throw msg.embed.send(message, "ОШИБКА", attachError, msg.colors.err);
            args["target"] = Steamid.parseSteamID2(attached.steam_id);
        };

        const Users = Workplace.findLRUsersStatsBySteamID(args);
        const Servers = Workplace.getLRServersList(args);

        return Promise.all([Users, Servers])
            .then(([Users, Servers]) => {

                if (Users < 1) throw "Не могу найти игрока";
                if (Servers < 1) throw "Не могу найти сервер(а)";


                Users.forEach(user => {

                    if (!user) return;

                    const attachmentObj = Ranks.LR_getRank(user.rank);

                    const table_name = Object.keys(user._modelOptions.sequelize.models)[0];
                    const server = Servers.find(server => server.table_name === table_name);

                    const body = [];

                    body.push(`**Nickname: ${user.name}**`);
                    body.push(`**STEAMID: ${user.steam}**`);

                    const fieldsObj = {};
                    fieldsObj.name = [];
                    fieldsObj.body = [];
                    fieldsObj.inline = true;

                    const fieldsNames = [
                        "Очки",
                        // "Ранг", // ? Removed
                        "Убийств",
                        "Смертей",
                        "Выстрелов",
                        "Попаданий",
                        "Хэдшотов",
                        "Помощь",
                        "Раундов Выиграно",
                        "Раундов Проиграно",
                        "Наиграно",
                        "Последнее Подключение",
                    ];

                    const keys = Object.keys(user.dataValues);
                    keys.splice(0, 2);
                    keys.splice(1, 1);
                    // keys.splice(3, 4);

                    keys.forEach((key, i) => {
                        fieldsObj.name.push(fieldsNames[i]);

                        if (key === "lastconnect") {
                            return fieldsObj.body.push(parseTime(user[key]));
                        }

                        if (key === "playtime") {
                            return fieldsObj.body.push(parseDate(user[key]));
                        }

                        fieldsObj.body.push(user[key]);
                    })

                    // todo: add function for get K/D (and etc)

                    // add K/D stats
                    fieldsObj.name.splice(3, 0, "У/С");
                    fieldsObj.body.splice(3, 0, (user.kills / user.deaths).toFixed(2));

                    // add K/D stats
                    fieldsObj.name.splice(6, 0, "Точность");
                    fieldsObj.body.splice(6, 0, ((user.hits * 100) / user.shoots).toFixed(2) + "%");

                    return msg.embed.send(message, server.server_name, body, msg.colors.send, null, attachmentObj, fieldsObj);

                })
            }).catch((err) => {
                console.log(err);
                msg.embed.send(message, "ОШИБКА", err, msg.colors.err);
                return err
            });


    }
}

const parseDate = (data) => {
    // ? Thx for https://stackoverflow.com/a/37096512/11109818
    const d = data;
    const h = Math.floor(d / 3600);
    const m = Math.floor(d % 3600 / 60);
    const s = Math.floor(d % 3600 % 60);

    const hd = h > 0 ? h + " Ч, " : "";
    const md = m > 0 ? m + " М, " : "";
    const sd = s > 0 ? s + " С" : "";
    return hd + md + sd;
};

const parseTime = (data) => {
    const date = new Date(data * 1000);
    return date.toLocaleDateString();
};

module.exports = CheckUserStats;