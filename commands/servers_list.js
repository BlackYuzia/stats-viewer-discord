const { Command } = require('discord-akairo');
const { Workplace, msg } = require('../index');

class ServersList extends Command {
    constructor() {
        super('ServersList', {
            aliases: ["servers_list", "sl", "ls", "serv_list"],
            channelRestriction: "guild",
            description: 'Позволяет получить список всех серверов',
            category: "statistic",
            options: {
                usage: `sl`,
                example: `sl`,
                flags: []
            },
            split: 'quoted',
            args: []
        });
    }

    exec(message, args) {
        // todo: refactory in next versions 
        args["gid"] = message.guild.id;
        args["type"] = message.util.prefix.slice(0, -1).toUpperCase();

        const title = `Список [${args["type"]}] Серверов`;
        const fields = {}
        fields.name = [];
        fields.body = [];

        if (args["type"] === "FPS") {
            const Servers = Workplace.getFPSServersList(args);

            return Servers
                .then((servers) => {
                    servers.forEach((server, i) => {
                        // todo: Do something with limit: 25 fields in one embed
                        if (i > 24) return console.log("[BUG] Too many servers :c");
                        fields.name.push(server.server_name);
                        fields.body.push(server.server_ip);
                    })

                    return msg.embed.send(message, title, null, msg.colors.send, null, null, fields);
                }).catch((err) => {
                    console.log("err", err);
                    msg.embed.send(message, "ОШИБКА", err, msg.colors.err, "Проверьте, разрешили ли вы доступ к базе с ip: 195.201.148.144");
                    return err;
                });
        }

        // if (args("type") === LR);

        const Servers = Workplace.getLRServersList(args);

        return Servers
            .then((servers) => {
                servers.forEach((server, i) => {
                    // todo: Do something with limit: 25 fields in one embed
                    if (i > 24) return console.log("[BUG] Too many servers :c");
                    fields.name.push(server.server_name);
                    fields.body.push(server.server_ip);
                })

                return msg.embed.send(message, title, null, msg.colors.send, null, null, fields);
            }).catch((err) => {
                console.log("err", err);
                msg.embed.send(message, "ОШИБКА", err, msg.colors.err, "Проверьте, разрешили ли вы доступ к базе с ip: 195.201.148.144");
                return err;
            });
    }
}

module.exports = ServersList;