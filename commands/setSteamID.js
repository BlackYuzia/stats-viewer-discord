const { Command } = require('discord-akairo');
const { Workplace, msg } = require('../index.js');

class SetSteamID extends Command {
    constructor() {
        super('SetSteamID', {
            aliases: [`sid`, "steamid"],
            category: "SteamID",
            description: 'Позволяет привязать SteamID к вашему discord аккаунту для быстрой работы',
            options: {
                usage: `sid <steamid>`,
                example: `sid 76561198843330785`,
                flags: []
            },
            split: 'quoted',
            args: [
                {
                    id: 'steam_id',
                    prompt: {
                        start: () => {
                            return msg.embed.reply('**Введите SteamID который вы хотите привязать к своему аккаунту discord.**', "Поддерживается любой формат STEAMID\n```SteamID64, SteamID3, SteamID2, AccountID```\nПривязка работает как для `LR` так и `FPS` Статистики.");
                        }
                    }
                }
            ]
        });
    }

    exec(message, args) {
        // todo: refactory in next versions 
        args["discord_id"] = message.author.id;

        const account = Workplace.updateOrCreateAccount(args);

        return account
            .then((acc) => {
                if (!acc) return;

                msg.embed.send(message, "Успешно", `Теперь ваш STEAMID: ${acc.steam_id}`, msg.colors.send);
            }).catch((err) => {
                return err;
            });
    }
}

module.exports = SetSteamID;