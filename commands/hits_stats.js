const { Command } = require('discord-akairo');
const { Workplace, Steamid, Ranks, msg } = require('../index');

const getPersent = (a, b) => ((b * 100) / a).toFixed(2) + "%";

class CheckUserHits extends Command {
    constructor() {
        super('CheckUserHits', {
            aliases: ['hits', "hit", "ht"],
            channelRestriction: "guild",
            category: "statistic",
            description: ['Позволяет посмотреть статистику игрока по попаданиям'],
            options: {
                usage: `hits ["STEAMID2" or "STEAMID64" or "STEAMID3"]`,
                example: `hits STEAM_0:123456789 | hits 1234567890`,
                flags: [],
                module_name: "ExStats Hits"
            },
            split: 'quoted',
            args: [
                {
                    id: 'target',
                    default: null
                }
            ]
        });
    }

    async exec(message, args) {
        // todo: refactory in next versions 
        args["gid"] = message.guild.id;
        args["discord_id"] = message.author.id;
        args["type"] = message.util.prefix.slice(0, -1).toUpperCase();

        // todo: fix bug with throw err
        const attachError = [
            "У вас не привязан STEAMID к аккаунту и вы не указали данные игрока.",
            "**Привяжите steamid** или **используйте команду с аргументами**."
        ];

        if (args.type === "FPS") {

            if (args["target"]) args["target"] = Steamid.parseAccountID(args["target"]);
            else {
                // todo: Оптимизировать в следующий версиях
                const attached = await Workplace.findAccount(args);
                if (!args["target"] && !attached) throw msg.embed.send(message, "ОШИБКА", attachError, msg.colors.err);
                args["target"] = Steamid.parseAccountID(attached.steam_id);
            };

            // Get account_id, nickname, steam_id(64), ip
            const userData = Workplace.findUserByAccountID(args);
            const ServersList = Workplace.getFPSServersList(args);
            const userStats = Workplace.findUserStatsByAccountID(args);
            const weaponsStats = Workplace.findWeaponsStatsByAccountID(args);

            return Promise.all([userData, ServersList, userStats, weaponsStats])
                .then(([userData, ServersList, userStats, weaponsStats]) => {

                    if (!userData) throw "Не могу найти ни одного игрока."
                    const userNickname = `**${userData.nickname}**`;
                    const userSteamID2 = `**SteamID: ${Steamid.parseSteamID2(userData.steam_id)}**`;

                    // todo: change to throw 
                    if (userStats < 1) {
                        const body = [];
                        body.push(userSteamID2);
                        body.push("```Не могу найти никакой статистики этого игрока.```");

                        return msg.embed.send(message, userNickname, body, msg.colors.err)
                    };

                    if (weaponsStats < 1) throw "Не могу найти статистику оружия игрока.";

                    userStats
                        .forEach(stat => {
                            const server = ServersList.find(server => server.id === stat.server_id);
                            const weapons = weaponsStats.filter(weapon => weapon.server_id === stat.server_id)
                            const body = [];
                            const fieldsNames = [
                                "Убийств",
                                "Хєдшотов",
                                "Голова",
                                "Тело",
                                "Руки",
                                "Ноги",
                                "Всего",
                                /* "kills",
                                "shoots",
                                "hits_head",
                                "hits_body",
                                "hits_left_arm",
                                "hits_right_arm",
                                "hits_left_leg",
                                "hits_right_leg",
                                "headshots" */
                            ]

                            // Rank getter Block 
                            const rankElement = --stat.rank;
                            const settingID = server.settings_rank_id;

                            const rankObject = {
                                gid: args.gid,
                                type: args.type,
                                settingID: settingID,
                                rankElement: rankElement
                            }

                            const attachmentObj = Ranks.FPS_getRank(rankObject);

                            body.push(`**Nickname:** ${userNickname}`);
                            body.push(userSteamID2);

                            const fieldsObj = {};
                            fieldsObj.name = [];
                            fieldsObj.body = [];
                            fieldsObj.inline = true;

                            // const fields = msg.fn.createFields(fieldsNames, fieldsBodies, true);

                            const hits = {};
                            hits.kills = 0;
                            hits.headshots = 0;
                            hits.head = 0;
                            hits.body = 0;
                            hits.arms = 0;
                            hits.legs = 0;
                            hits.total = 0;
                            hits.shoots = 0;

                            weapons
                                .forEach(weapon => {
                                    hits.kills += parseInt(weapon.kills, 10);
                                    hits.headshots += parseInt(weapon.headshots, 10);
                                    hits.head += parseInt(weapon.hits_head, 10);
                                    hits.head += parseInt(weapon.hits_neck, 10);
                                    hits.body += parseInt(weapon.hits_chest, 10);
                                    hits.body += parseInt(weapon.hits_stomach, 10);
                                    hits.arms += parseInt(weapon.hits_left_arm, 10);
                                    hits.arms += parseInt(weapon.hits_right_arm, 10);
                                    hits.legs += parseInt(weapon.hits_left_leg, 10);
                                    hits.legs += parseInt(weapon.hits_right_leg, 10);
                                    hits.shoots += parseInt(weapon.shoots, 10);
                                });

                            hits.total += hits.head;
                            hits.total += hits.body;
                            hits.total += hits.arms;
                            hits.total += hits.legs;

                            const hitsKeys = Object.keys(hits);
                            hitsKeys
                                .forEach(key => {
                                    if (key === "kills" || key === "headshots" || key === "total" || key === "shoots") return;
                                    hits[key] = getPersent(hits.total, hits[key]);
                                })

                            hitsKeys
                                .forEach((key, i) => {
                                    if (key === "shoots") return;

                                    fieldsObj.name.push(fieldsNames[i]);
                                    fieldsObj.body.push(hits[key]);
                                })

                            // add accuracy
                            fieldsObj.name.push("Точность");
                            fieldsObj.body.push(((hits.total * 100) / hits.shoots).toFixed(2) + "%");

                            return attachmentObj
                                .then((attachmentObj) => {
                                    return msg.embed.send(message, server.server_name, body, msg.colors.send, null, attachmentObj, fieldsObj);
                                })

                        })
                })
                .catch((err) => {
                    console.log("err", err);
                    if (err.name === "SequelizeDatabaseError") {
                        msg.embed.send(message, "ОШИБКА", `Не установлен модуль [\`${this.options.module_name}\`] для [\`${args.type}\`]`, msg.colors.err, "Установите модуль");
                        return err;
                    }
                    msg.embed.send(message, "ОШИБКА", err, msg.colors.err, "Проверьте, разрешили ли вы доступ к базе с ip: 195.201.148.144");
                    return err;
                });;

        }

        // if (args["type"] === "LR")        

        // todo: (по идее, можно) вывести в функцию проверки.
        if (args["target"]) args["target"] = Steamid.parseSteamID2(args["target"]);
        else {
            // todo: Оптимизировать в следующий версиях
            const attached = await Workplace.findAccount(args);
            if (!args["target"] && !attached) throw msg.embed.send(message, "ОШИБКА", attachError, msg.colors.err);
            args["target"] = Steamid.parseSteamID2(attached.steam_id);
        };

        const Users = Workplace.findLRUsersStatsBySteamID(args);
        const Servers = Workplace.getLRServersList(args);
        const Hits = Workplace.LR_getUserHitsStats(args);

        return Promise.all([Users, Servers, Hits])
            .then(([Users, Servers, Hits]) => {

                if (Users < 1) throw "Не могу найти игрока";
                if (Servers < 1) throw "Не могу найти сервер(а)";
                if (Hits < 1) throw "Не могу найти необычные убийства";

                Hits = Hits.flat()

                Users.forEach(user => {

                    if (!user) return;

                    const attachmentObj = Ranks.LR_getRank(user.rank);

                    const table_name = Object.keys(user._modelOptions.sequelize.models)[0];
                    const server = Servers.find(server => server.table_name === table_name);
                    const hit = Hits.find(hit => Object.keys(hit._modelOptions.sequelize.models)[0] === table_name + "_hits");

                    const body = [];

                    body.push(`**Nickname: ${user.name}**`);
                    body.push(`**STEAMID: ${user.steam}**`);

                    const fieldsObj = {};
                    fieldsObj.name = [];
                    fieldsObj.body = [];
                    fieldsObj.inline = true;

                    fieldsObj.name.push("Убийств");
                    fieldsObj.body.push(user.kills);

                    /*                     const hitNames = [
                                            "Урон ХП",
                                            "Урон по Броне",
                                            "Голова",
                                            "Тело",
                                            "Руки",
                                            "Ноги",
                                            // "DmgHealth",
                                            // "DmgArmor",
                                            // "Head",
                                            // "Chest",
                                            // "Belly",
                                            // "LeftArm",
                                            // "RightArm",
                                            // "LeftLeg",
                                            // "RightLeg",
                                            // "Neak",
                                        ]; */

                    /* 
                        Схема:
                        Голова, шея === Голова
                        Тело, живот === Тело
                        Левая, правая Рука === Руки
                        Левая, правая Нога === Ноги
                    */

                    fieldsObj.name.push("Урон");
                    fieldsObj.body.push(hit.DmgHealth);
                    fieldsObj.name.push("Урон по Броне");
                    fieldsObj.body.push(hit.DmgArmor);

                    const keys = Object.keys(Hits[0].dataValues);
                    // Remove SteamID, dmgHP, dmgArmor keys
                    keys.splice(0, 3);

                    let allHits = 0;

                    keys
                        .forEach((key) => {
                            allHits += parseInt(hit[key]);
                        });

                    // todo: refactory in next versions
                    fieldsObj.name.push("Голова");
                    fieldsObj.body.push(getPersent(allHits, hit.Head + hit.Neak));
                    fieldsObj.name.push("Тело");
                    fieldsObj.body.push(getPersent(allHits, hit.Chest + hit.Belly));
                    fieldsObj.name.push("Руки");
                    fieldsObj.body.push(getPersent(allHits, hit.LeftArm + hit.RightArm));
                    fieldsObj.name.push("Ноги");
                    fieldsObj.body.push(getPersent(allHits, hit.LeftLeg + hit.RightLeg));
                    fieldsObj.name.push("Всего");
                    fieldsObj.body.push(allHits);
                    fieldsObj.name.push("Точность");
                    fieldsObj.body.push((allHits * 100 / user.shoots).toFixed(2) + "%");

                    return msg.embed.send(message, server.server_name, body, msg.colors.send, null, attachmentObj, fieldsObj);

                })
            }).catch((err) => {
                console.log(err);
                if (err.name === "SequelizeDatabaseError") {
                    msg.embed.send(message, "ОШИБКА", `Не установлен модуль [\`${this.options.module_name}\`] для [\`${args.type}\`]`, msg.colors.err, "Установите модуль");
                    return err;
                }
                msg.embed.send(message, "ОШИБКА", err, msg.colors.err, "Проверьте, разрешили ли вы доступ к базе с ip: 195.201.148.144");
                return err
            });


    }
}

module.exports = CheckUserHits;