const { Command } = require('discord-akairo');
const { Workplace, Steamid, Ranks, msg } = require('../index');

class CheckUserKills extends Command {
    constructor() {
        super('CheckUserKills', {
            aliases: ['kills', "kill", "kl"],
            channelRestriction: "guild",
            category: "statistic",
            description: 'Позволяет посмотреть статистику игрока по убийствам',
            options: {
                usage: `stats ["STEAMID2" or "STEAMID64" or "STEAMID3"]`,
                example: `stats STEAM_0:123456789 | stats 1234567890`,
                flags: [],
                module_name: "Unusual Kills | Необычные Убийства"
            },
            split: 'quoted',
            args: [
                {
                    id: 'target',
                    default: null
                }
            ]
        });
    }

    async exec(message, args) {
        // todo: refactory in next versions 
        args["gid"] = message.guild.id;
        args["discord_id"] = message.author.id;
        args["type"] = message.util.prefix.slice(0, -1).toUpperCase();

        // todo: fix bug with throw err
        const attachError = [
            "У вас не привязан STEAMID к аккаунту и вы не указали данные игрока.",
            "**Привяжите steamid** или **используйте команду с аргументами**."
        ];

        if (args.type === "FPS") {
            if (args["target"]) args["target"] = Steamid.parseAccountID(args["target"]);
            else {
                // todo: Оптимизировать в следующий версиях
                const attached = await Workplace.findAccount(args);
                if (!args["target"] && !attached) throw msg.embed.send(message, "ОШИБКА", attachError, msg.colors.err);
                args["target"] = Steamid.parseAccountID(attached.steam_id);
            };

            const userData = Workplace.findUserByAccountID(args);
            const ServersList = Workplace.getFPSServersList(args);
            const userStats = Workplace.findUserStatsByAccountID(args);
            const Unusuals = Workplace.findFPSUnusualKillsStatsByAccountID(args);

            return Promise.all([userData, ServersList, userStats, Unusuals])
                .then(([userData, ServersList, userStats, Unusuals]) => {

                    if (!userData) throw "Не могу найти ни одного игрока."
                    const userNickname = `**${userData.nickname}**`;
                    const userSteamID2 = `**SteamID: ${Steamid.parseSteamID2(userData.steam_id)}**`;

                    if (ServersList < 1) throw "Не могу найти сервера.";
                    if (userStats < 1) throw "Не могу найти никакой статистики этого игрока.";
                    if (Unusuals < 1) throw "Не могу найти никакой статистики этого игрока.";

                    userStats
                        .forEach(user => {

                            const server = ServersList.find(server => server.id === user.server_id);
                            const unusual = Unusuals.find(unusual => unusual.server_id === user.server_id);

                            const body = [];
                            const fieldsNames = [
                                "Первое убийство",
                                "Прострел",
                                "Без Зума",
                                "На Бегу",
                                "В Прыжке",
                                "Слепым",
                                "В Смок",
                                "С Разворота",
                                "Последней Пулей"
                            ]

                            // Rank getter Block 
                            const rankElement = --user.rank;
                            const settingID = server.settings_rank_id;

                            const rankObject = {
                                gid: args.gid,
                                type: args.type,
                                settingID: settingID,
                                rankElement: rankElement
                            }

                            const attachmentObj = Ranks.FPS_getRank(rankObject);

                            body.push(`**Nickname:** ${userNickname}`);
                            body.push(userSteamID2);

                            const fieldsObj = {};
                            fieldsObj.name = [];
                            fieldsObj.body = [];
                            fieldsObj.inline = true;

                            const keys = Object.keys(Unusuals[0].dataValues);
                            // remove id, acc_id, serv_id
                            keys.splice(0, 3);

                            fieldsObj.name.push("Убийств");
                            fieldsObj.body.push(user.kills);
                            fieldsObj.name.push("Смертей");
                            fieldsObj.body.push(user.deaths);
                            fieldsObj.name.push("У/С");
                            fieldsObj.body.push((user.kills / user.deaths).toFixed(2));

                            keys.forEach((key, i) => {
                                if (!unusual || !unusual[key]) return;
                                fieldsObj.name.push(fieldsNames[i]);
                                fieldsObj.body.push(unusual[key]);
                            })

                            return attachmentObj
                                .then((attachmentObj) => {
                                    return msg.embed.send(message, server.server_name, body, msg.colors.send, null, attachmentObj, fieldsObj);
                                })

                        })

                }).catch((err) => {
                    console.log("err", err);
                    if (err.name === "SequelizeDatabaseError") {
                        msg.embed.send(message, "ОШИБКА", `Не установлен модуль [\`${this.options.module_name}\`] для [\`${args.type}\`]`, msg.colors.err, "Установите модуль");
                        return err;
                    }
                    msg.embed.send(message, "ОШИБКА", err, msg.colors.err, "Проверьте, разрешили ли вы доступ к базе с ip: 195.201.148.144");
                    return err;
                });
        }

        // if (args["type"] === "LR")        

        // todo: (по идее, можно) вывести в функцию проверки.
        if (args["target"]) args["target"] = Steamid.parseSteamID2(args["target"]);
        else {
            // todo: Оптимизировать в следующий версиях
            const attached = await Workplace.findAccount(args);
            if (!args["target"] && !attached) throw msg.embed.send(message, "ОШИБКА", attachError, msg.colors.err);
            args["target"] = Steamid.parseSteamID2(attached.steam_id);
        };

        const Users = Workplace.findLRUsersStatsBySteamID(args);
        const Servers = Workplace.getLRServersList(args);
        const Unusuals = Workplace.LR_getUserUnusualKillsStats(args);

        return Promise.all([Users, Servers, Unusuals])
            .then(([Users, Servers, Unusuals]) => {

                if (Users < 1) throw "Не могу найти игрока";
                if (Servers < 1) throw "Не могу найти сервер(а)";
                if (Unusuals < 1) throw "Не могу найти необычные убийства";

                Unusuals = Unusuals.flat()

                Users.forEach(user => {

                    if (!user) return;

                    const attachmentObj = Ranks.LR_getRank(user.rank);

                    const table_name = Object.keys(user._modelOptions.sequelize.models)[0];
                    const server = Servers.find(server => server.table_name === table_name);
                    const unusual = Unusuals.find(unusual => Object.keys(unusual._modelOptions.sequelize.models)[0] === table_name + "_unusualkills");

                    const body = [];

                    body.push(`**Nickname: ${user.name}**`);
                    body.push(`**STEAMID: ${user.steam}**`);

                    const fieldsObj = {};
                    fieldsObj.name = [];
                    fieldsObj.body = [];
                    fieldsObj.inline = true;

                    fieldsObj.name.push("Убийств");
                    fieldsObj.body.push(user.kills);
                    fieldsObj.name.push("Смертей");
                    fieldsObj.body.push(user.deaths);

                    fieldsObj.name.push("У/С");
                    fieldsObj.body.push((user.kills / user.deaths).toFixed(2));

                    const killsNames = [
                        "Первое убийство",
                        "Прострел",
                        "Без Зума",
                        "На Бегу",
                        "В Прыжке",
                        "Слепым",
                        "В Смок",
                        "С Разворота",
                        "Последней Пулей"
                    ];

                    const keys = Object.keys(Unusuals[0].dataValues);
                    // Remove SteamID key
                    keys.splice(0, 1);

                    keys
                        .forEach((key, i) => {
                            fieldsObj.name.push(killsNames[i]);
                            fieldsObj.body.push(unusual[key]);
                        })

                    return msg.embed.send(message, server.server_name, body, msg.colors.send, null, attachmentObj, fieldsObj);

                })
            }).catch((err) => {
                console.log(err);
                if (err.name === "SequelizeDatabaseError") {
                    msg.embed.send(message, "ОШИБКА", `Не установлен модуль [\`${this.options.module_name}\`] для [\`${args.type}\`]`, msg.colors.err, "Установите модуль");
                    return err;
                }
                msg.embed.send(message, "ОШИБКА", err, msg.colors.err);
                return err
            });


    }
}

module.exports = CheckUserKills;