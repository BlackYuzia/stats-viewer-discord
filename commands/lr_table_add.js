const { Command } = require('discord-akairo');
const { Workplace, msg, config } = require('../index.js');

class LR_Table_Add extends Command {
    constructor() {
        super('LR_Table_Add', {
            aliases: ['add_table', "addtable"],
            channelRestriction: "guild",
            prefix: ["lr_"],
            category: "lr tables",
            description: 'Позволяет привязать несколько таблиц к одной базе данных LR статистики.',
            options: {
                usage: `add_table <table_name> <server_name> <server_ip:port>`,
                example: `add_table lvl_base "MY COOL SERVER" 0.0.0.0:27015`,
                flags: [
                    "--old - в данный момент, позволяет использовать старый синтаксис для `_weapons` таблиц(ы)"
                ]
            },
            split: 'quoted',
            args: [
                {
                    id: 'table_name',
                    prompt: {
                        start: () => {
                            return msg.embed.reply('**Введите имя таблицы с сервером.**');
                        }
                    }
                },
                {
                    id: 'server_name',
                    prompt: {
                        start: () => {
                            return msg.embed.reply('**Введите имя(название) сервера.**');
                        }
                    }
                },
                {
                    id: 'server_ip',
                    prompt: {
                        start: () => {
                            return msg.embed.reply('**Введите данные от сервера (IP:PORT).**');
                        }
                    }
                },
                {
                    id: 'old_syntax',
                    match: "flag",
                    prefix: '--old'
                }
            ]
        });
    }

    userPermissions(message) {
        const check = message.member.roles.cache.find(role => role.name === config.Permissions.role.name);
        // todo: throw this to .catch(err)
        if (!check) msg.embed.send(message, "ОШИБКА", `У вас нет роли с именем \`${config.Permissions.role.name}\`\nСоздайте и/или выдайте ее себе`, msg.colors.err);
        return check;
    }

    exec(message, args) {
        // todo: refactory in next versions 
        args["gid"] = message.guild.id;
        args["type"] = message.util.prefix.slice(0, -1).toUpperCase();

        return Workplace.addLRTable(args)
            .then((data) => {
                if (!data) throw "Что-то пошло не так";

                const title = `Успешно добавлена таблица \`${args["table_name"]}\``;
                return msg.embed.send(message, title, null, msg.colors.send);
            }).catch((err) => {
                console.log(this.id, err);
                // todo: добавить доп. инфу в футер .
                msg.embed.send(message, "ОШИБКА", err, msg.colors.err);
                return err;
            });


    }
}

module.exports = LR_Table_Add;