const { Command } = require('discord-akairo');
const { Workplace, msg, config } = require('../index.js');

class SetupDatabase extends Command {
    constructor() {
        super('SetupDatabase', {
            aliases: ['add_database', "add_db", "adb"],
            channelRestriction: "guild",
            category: "database",
            description: 'Позволяет привязать базу данных с серверами к каналу.',
            options: {
                usage: `add_db <db_host> <db_name> <db_user> <db_pass> <db_dialect> <db_port>`,
                example: `add_db kruzyaPidor.ru fps_database fps_user MySecretPassword mysql 3306`,
                flags: []
            },
            split: 'quoted',
            args: [
                {
                    id: 'host',
                    prompt: {
                        start: () => {
                            return msg.embed.reply('**Введите ссылку на хостинг с Базой Данных.**');
                        }
                    }
                },
                {
                    id: 'name',
                    prompt: {
                        start: () => {
                            return msg.embed.reply('**Введите имя от Базы Данных.**');
                        }
                    }
                },
                {
                    id: 'user',
                    prompt: {
                        start: () => {
                            return msg.embed.reply('**Введите пользователя для Базы Данных.**');
                        }
                    }
                },
                {
                    id: 'pass',
                    prompt: {
                        start: () => {
                            return msg.embed.reply('**Введите пароль от Базы Данных.**');
                        }
                    }
                },
                {
                    id: 'dialect',
                    prompt: {
                        start: () => {
                            return msg.embed.reply('**Введите диалект от Базы Данных.** [По умолчанию: mysql]');
                        }
                    }
                },
                {
                    id: 'port',
                    prompt: {
                        start: () => {
                            return msg.embed.reply('**Введите порт от Базы Данных.** [По умолчанию: 3306]');
                        }
                    }
                }
                // todo: fix exploit or do better
                /* ,
                {
                    id: 'gid',
                    type: "guild",
                    default: message => message.guild.id
                } */
            ]
        });
    }

    userPermissions(message) {
        const check = message.member.roles.cache.find(role => role.name === config.Permissions.role.name);
        // todo: throw this to .catch(err)
        if (!check) msg.embed.send(message, "ОШИБКА", `У вас нет роли с именем \`${config.Permissions.role.name}\`\nСоздайте и/или выдайте ее себе`, msg.colors.err);
        return check;
    }

    exec(message, args) {
        // BUG
        // todo: исправить баг-фичу с по аргументным вводом, при котором не удаляются введеные аргументы.
        // Удаление сообщения с данными от БД 
        message.delete();
        // todo: refactory in next versions 
        args["gid"] = message.guild.id;
        args["type"] = message.util.prefix.slice(0, -1).toUpperCase();

        Workplace.findOrCreateGuild(args["gid"]);

        return Workplace.createDatabaseSetup(args)
            .then(() => {
                const title = `База для [\`${args["type"]}\`] статистики успешно добавлена`;

                return msg.embed.send(message, title, null, msg.colors.send);
            }).catch((err) => {
                msg.embed.send(message, "Ошибка", "Что-то не так...", msg.colors.err);
                console.log("err", err);
                return err;
            });
    }
}

module.exports = SetupDatabase;