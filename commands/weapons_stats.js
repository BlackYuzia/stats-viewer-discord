const { Command } = require('discord-akairo');
const { Workplace, Steamid, msg } = require('../index');

class CheckWeaponsStats extends Command {
    constructor() {
        super('CheckWeaponsStats', {
            aliases: ['weapons', "weapons_stats", "ws"],
            channelRestriction: "guild",
            category: "statistic",
            description: 'Позволяет посмотреть статистику оружия игрока (FPS/LR)',
            options: {
                usage: `ws ["STEAMID2" or "STEAMID64" or "STEAMID3"]`,
                example: `ws STEAM_0:123456789 | stats 1234567890`,
                flags: [],
                module_name: "_weapons"
            },
            split: 'quoted',
            args: [
                {
                    id: 'target',
                    default: null
                }
            ]
        });
    }

    async exec(message, args) {
        // todo: refactory in next versions 
        args["gid"] = message.guild.id;
        args["discord_id"] = message.author.id;
        args["type"] = message.util.prefix.slice(0, -1).toUpperCase();

        const attachError = [
            "У вас не привязан STEAMID к аккаунту и вы не указали данные игрока.",
            "**Привяжите steamid** или **используйте команду с аргументами**."
        ]

        if (args.type === "FPS") {
            if (args["target"]) args["target"] = Steamid.parseAccountID(args["target"]);
            else {
                // todo: Оптимизировать в следующий версиях
                const target = await Workplace.findAccount(args);
                if (!args["target"] && !target) throw msg.embed.send(message, "ОШИБКА", attachError, msg.colors.err);
                args["target"] = Steamid.parseAccountID(target.steam_id);
            };

            // Get account_id, nickname, steam_id(64), ip
            const userData = Workplace.findUserByAccountID(args);
            const ServersList = Workplace.getFPSServersList(args);
            const WeaponsStats = Workplace.findWeaponsStatsByAccountID(args);

            return Promise.all([userData, ServersList, WeaponsStats])
                .then((arr) => {
                    // todo: remove this, change to then([data, data, data])
                    const data = {
                        userData: arr[0],
                        ServersList: arr[1],
                        WeaponsStats: arr[2]
                    };

                    if (!data.userData) throw "Не могу найти ни одного игрока.";
                    data.userNickname = `**${data.userData.nickname}**`;
                    data.userSteamID2 = `**SteamID: ${Steamid.parseSteamID2(data.userData.steam_id)}**`;

                    if (data.ServersList < 1) {
                        const body = [];
                        body.push(data.userSteamID2);
                        body.push("```Не могу найти сервера.```");

                        return msg.embed.send(message, data.userNickname, body, msg.colors.err)
                    };

                    if (data.WeaponsStats < 1) {
                        const body = [];
                        body.push(data.userSteamID2);
                        body.push("```Не могу найти ни одного оружия с статистикой для этого игрока```");

                        return msg.embed.send(message, data.userNickname, body, msg.colors.err)
                    };

                    /* 
                     todo: add new functional to weapons command
                    Оружие: {weapon_name} | У: {убийств} | Х: {хэдшотов} | Т: {точность (высчитываем по % попаданиях к убийствам)} 

                    Flags:
                    --hits (или -h) - выводить также и попадания => head, neck, chest, arms, legs
                    */

                    const fieldsNames = [
                        "Оружие",
                        "Убийств",
                        "Выстрелов", // todo: поменять "Выстрелы", на "Хедшоты"
                        /* 
                        ? Removed,
                        // 'cause kruzya has been pidor, I joking...
                        ? `cause too many symbols in one embed msg.
                        "Попаданий в голову",
                        "Попаданий в тело",
                        "Попаданий в левую руку",
                        "Попаданий в правую руку",
                        "Попаданий в левую ногу",
                        "Попаданий в правую ногу",
                        "Попаданий в голову", */
                        /* "weapon",
                        "kills",
                        "shoots",
                        "hits_head",
                        "hits_body",
                        "hits_left_arm",
                        "hits_right_arm",
                        "hits_left_leg",
                        "hits_right_leg",
                        "headshots", */
                    ];

                    const keys = Object.keys(data.WeaponsStats[0].dataValues);

                    // Remove id, account_id, server_id keys
                    keys.splice(0, 3);
                    keys.splice(3, keys.length);

                    data.ServersList
                        .forEach(server => {
                            const Weapons = data.WeaponsStats.filter(weapon => {
                                if (weapon.server_id === server.id) return weapon;
                            })

                            if (Weapons.length < 1) return;

                            const body = [];

                            body.push(`**Nickname: ${data.userData.nickname}**\n`)
                            body.push(`**SteamID: ${Steamid.parseSteamID2(data.userData.steam_id)}**`)

                            Weapons.forEach(weapon => {
                                body.push("```")
                                keys.forEach((key, i) => {
                                    body.push(`${fieldsNames[i]}: ${weapon[key]} |`);
                                })
                                body.push("```")
                            })

                            msg.embed.send(message, server.server_name, body.join(" "), msg.colors.send);
                        });
                }).catch((err) => {
                    console.log("err", err);
                    if (err.name === "SequelizeDatabaseError") {
                        msg.embed.send(message, "ОШИБКА", `Не установлен модуль [\`${this.options.module_name}\`] для [\`${args.type}\`]`, msg.colors.err, "Установите модуль");
                        return err;
                    }
                    msg.embed.send(message, "ОШИБКА", err, msg.colors.err, "Проверьте, разрешили ли вы доступ к базе с ip: 195.201.148.144");
                    return err;
                });
        }

        // if (args.type === "LR") {};

        if (args["target"]) args["target"] = Steamid.parseSteamID2(args["target"]);
        else {
            // todo: Оптимизировать в следующий версиях
            const target = await Workplace.findAccount(args);
            if (!args["target"] && !target) throw msg.embed.send(message, "ОШИБКА", attachError, msg.colors.err);
            args["target"] = Steamid.parseSteamID2(target.steam_id);
        };

        const Users = Workplace.findLRUsersStatsBySteamID(args);
        const Weapons = Workplace.findLRUsersWeaponsStatsBySteamID(args);
        const Servers = Workplace.getLRServersList(args);

        return Promise.all([Users, Weapons, Servers])
            .then(([Users, Weapons, Servers]) => {


                if (Users < 1) throw "Не могу найти игрока(ов)";
                if (Weapons < 1) throw "Не могу найти оружие(я)";
                if (Servers < 1) throw "Не могу найти сервер(а)";

                Weapons = Weapons.flat()

                Users.forEach(user => {

                    if (!user) return;

                    const table_name = Object.keys(user._modelOptions.sequelize.models)[0];
                    const server = Servers.find(server => server.table_name === table_name);
                    const weapons = Weapons.filter(weapon => {
                        const weapon_table_name = Object.keys(weapon._modelOptions.sequelize.models)[0];
                        if (table_name + "_weapons" === weapon_table_name) return weapon;
                    })

                    const body = [];

                    body.push(`**Nickname: ${user.name}**\n`);
                    body.push(`**STEAMID: ${user.steam}**`);

                    const fieldsNames = [
                        "Оружие",
                        "Убийств"
                    ];

                    if (!server.old_syntax) {
                        const keys = Object.keys(weapons[0].dataValues);
                        keys.splice(0, 1);

                        weapons.forEach(weapon => {
                            body.push("```")
                            keys.forEach((key, i) => {
                                body.push(`${fieldsNames[i]}: ${weapon[key]}`);
                            })
                            body.push("```")
                        })

                    }
                    else {

                        const keys = Object.keys(weapons[0].dataValues);
                        keys.splice(0, 3);

                        weapons.forEach(weapon => {
                            keys.forEach((key) => {
                                if (parseInt(weapon[key], 10)) {
                                    body.push("```");
                                    body.push(`${fieldsNames[0]}: ${key.replace("weapon_", "")} | ${fieldsNames[1]}: ${weapon[key]}`);
                                    body.push("```");
                                }
                            })
                        })

                    }

                    return msg.embed.send(message, server.server_name, body.join(" "), msg.colors.send);

                })

                return true;
            }).catch((err) => {
                console.log("err", err);
                if (err.name === "SequelizeDatabaseError") {
                    msg.embed.send(message, "ОШИБКА", `Не установлен модуль [\`${this.options.module_name}\`] для [\`${args.type}\`]`, msg.colors.err, "Установите модуль");
                    return err;
                }
                msg.embed.send(message, "ОШИБКА", err, msg.colors.err, "Проверьте, разрешили ли вы доступ к базе с ip: 195.201.148.144");
                return err;
            });


    }
}

module.exports = CheckWeaponsStats;