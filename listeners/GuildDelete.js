const { Listener } = require('discord-akairo');

class OnLeftListener extends Listener {
    constructor() {
        super('onLeft', {
            emitter: 'client',
            eventName: 'guildDelete'
        });
    }

    exec(guild) {
        console.log(`Bot was kicked from #${guild.name}:${guild.id}`);
    }
}

module.exports = OnLeftListener;