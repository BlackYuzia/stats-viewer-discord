const { Listener } = require('discord-akairo');
const { Workplace } = require('../index');

class onJoinListener extends Listener {
    constructor() {
        super('onJoin', {
            emitter: 'client',
            eventName: 'guildCreate'
        });
    }

    exec(guild) {
        // Add guild id to bot database 
        Workplace.findOrCreateGuild(guild.id);


        // If role don't exist, create role.
        if (!guild.roles.cache.find(role => role.name === config.Permissions.role.name)) {
            guild.createRole({
                name: config.Permissions.role.name,
                color: config.Permissions.role.color,
                mentionable: true // why not?
            })
                .then(role => console.log(`Успешно создана роль ${role.name} в канале: #${guild.name}:${guild.id}`))
                .catch(err => console.error(err));
        }

        console.log(`Bot was joined to #${guild.name}:${guild.id}`);
    }
}

module.exports = onJoinListener;