module.exports = function (sequelize, DataTypes) {
  return sequelize.define('fps_ranks', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    rank_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    rank_name: {
      type: DataTypes.STRING(128),
      allowNull: false
    },
    points: {
      type: DataTypes.FLOAT,
      allowNull: false
    }
  }, { timestamps: false });
};
