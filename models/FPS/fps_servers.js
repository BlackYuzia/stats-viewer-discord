module.exports = function (sequelize, DataTypes) {
  return sequelize.define('fps_servers', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    server_name: {
      type: DataTypes.STRING(256),
      allowNull: false
    },
    settings_rank_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    settings_points_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    server_ip: {
      type: DataTypes.STRING(32),
      allowNull: false
    }
  }, { timestamps: false });
};
