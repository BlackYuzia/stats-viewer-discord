module.exports = function (sequelize, DataTypes) {
  return sequelize.define('fps_players', {
    account_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    steam_id: {
      type: DataTypes.STRING(64),
      allowNull: false
    },
    nickname: {
      type: DataTypes.STRING(256),
      allowNull: false
    },
    ip: {
      type: DataTypes.STRING(24),
      allowNull: false
    }
  }, { timestamps: false });
};
