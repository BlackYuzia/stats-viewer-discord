module.exports = function (sequelize, DataTypes, table_name) {
  return sequelize.define(table_name, {
    steam: {
      type: DataTypes.STRING(32),
      allowNull: false,
      primaryKey: true,
      defaultValue: ''
    },
    name: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ''
    },
    value: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    rank: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    kills: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    deaths: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    shoots: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    hits: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    headshots: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    assists: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    round_win: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    round_lose: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    playtime: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    lastconnect: {
      type: DataTypes.DECIMAL,
      allowNull: true
    }
  }, { timestamps: false, freezeTableName: true });
};
