module.exports = function (sequelize, DataTypes, table_name) {
  return sequelize.define(`${table_name}_weapons`, {
    steam: {
      type: DataTypes.STRING(32),
      allowNull: false,
      primaryKey: true,
      defaultValue: ''
    },
    name: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ''
    },
    lastconnect: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_knife: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_taser: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_inferno: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_hegrenade: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_glock: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_hkp2000: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_tec9: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_usp_silencer: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_p250: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_cz75a: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_fiveseven: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_elite: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_revolver: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_deagle: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_negev: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_m249: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_mag7: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_sawedoff: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_nova: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_xm1014: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_bizon: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_mac10: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_ump45: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_mp9: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_mp7: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_p90: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_galilar: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_famas: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_ak47: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_m4a1: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_m4a1_silencer: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_aug: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_sg556: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_ssg08: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_awp: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_scar20: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_g3sg1: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_usp: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_p228: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_m3: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_tmp: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_mp5navy: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_galil: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_scout: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_sg550: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_sg552: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weapon_mp5sd: {
      type: DataTypes.DECIMAL,
      allowNull: true
    }
  }, { timestamps: false, freezeTableName: true });
};
