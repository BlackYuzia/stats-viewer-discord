module.exports = function (sequelize, DataTypes, table_name) {
  return sequelize.define(`${table_name}_weapons`, {
    steam: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: '',
      primaryKey: true
    },
    classname: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: '',
      primaryKey: true
    },
    kills: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    }
  }, { timestamps: false });
};
