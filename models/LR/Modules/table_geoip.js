module.exports = function (sequelize, DataTypes, table_name) {
  return sequelize.define(`${table_name}_geoip`, {
    steam: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: '',
      primaryKey: true
    },
    clientip: {
      type: DataTypes.STRING(16),
      allowNull: false,
      defaultValue: ''
    },
    country: {
      type: DataTypes.STRING(48),
      allowNull: false,
      defaultValue: ''
    },
    region: {
      type: DataTypes.STRING(48),
      allowNull: false,
      defaultValue: ''
    },
    city: {
      type: DataTypes.STRING(48),
      allowNull: false,
      defaultValue: ''
    },
    country_code: {
      type: DataTypes.STRING(4),
      allowNull: false,
      defaultValue: ''
    }
  }, { timestamps: false });
};
