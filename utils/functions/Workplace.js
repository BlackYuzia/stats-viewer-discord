const { Guilds, Databases, Accounts, LR_Tables, DataTables, config } = require('../../index.js');
const { AES } = require('crypto-js');
// const Op = Sequelize.Op;

/**
 * create and/or return guild data from Guilds table
 * @param {String} guildID string with guild id
 */
function findOrCreateGuild(guildID) {
    return Guilds.findOrCreate({
        where: {
            id: guildID
        }
    })
        .then((guild) => {
            return guild[0];
        }).catch((err) => {
            throw err
        });
};

/**
 * return created and/or updated Database data from database
 * @param {Object} data Object with guild id and database_name
 */
function createDatabaseSetup(data) {
    return Databases
        .findOrCreate({
            where: {
                gid: data.gid,            // Guild ID
                type: data.type            // DB type
            }
        })
        .then((database) => {
            // Encrypt pass
            const encrypt = AES.encrypt(data.pass, config.crypto).toString();

            return database[0]
                .update({
                    host: data.host,          // db host
                    database: data.name,      // db name
                    user: data.user,          // db user
                    pass: encrypt,            // encrypted db pass
                    port: data.port,          // db port
                    dialect: data.dialect     // db dialect
                })
                .then((data) => {
                    return data;
                }).catch((err) => {
                    throw err
                });
        }).catch((err) => {
            throw err
        });
};

/**
 * return removed data 
 * @param {Object} data Object with guild ID and database name
 */
function destroyDatabaseSetup(data) {
    return Databases
        .destroy({
            where: {
                gid: data.gid,          // Guild ID
                type: data.type          // DB Type
            }
        })
};

/**
 *
 *
 * @param {*} data
 * @returns
 */
function findUserByAccountID(data) {
    const PlayerTable = DataTables.getFpsPlayersTable(data);

    return PlayerTable
        .then((Table) => {
            return Table
                .findOne({
                    where: { account_id: data.target }
                })
        }).catch((err) => {
            throw err
        });
};

/**
 * return array ...
 * @param {Object} data
 * @returns array[]
 */
function findUserStatsByAccountID(data) {
    const PlayersStatsTable = DataTables.getFpsServersStatsTable(data);

    return PlayersStatsTable
        .then((Table) => {
            return Table
                .findAll({
                    where: { account_id: data.target }
                })
        }).catch((err) => {
            throw err
        });
};
/**
 * return all finded servers attached to this guild (id)
 * @param {String} data 
 */
function getFPSServersList(data) {
    const ServersTable = DataTables.getFpsServersTable(data);
    return ServersTable
        .then((Table) => {
            return Table.findAll();
        }).catch((err) => {
            throw err
        });
};

/** 
 * @param data
 * @return array[]
*/
function findWeaponsStatsByAccountID(data) {
    const Table = DataTables.getFpsWeaponsStatsTable(data);

    return Table
        .then((table) => {
            return table.findAll({
                where: {
                    account_id: data.target
                }
            })
        }).catch((err) => {
            throw err
        });
};


/**
 *
 *
 * @param {*} data
 * @returns {Array} Array[]
 */
function findFPSUnusualKillsStatsByAccountID(data) {
    const Table = DataTables.getFpsUnusualKillsTable(data);

    return Table
        .then((table) => {
            return table.findAll({
                where: {
                    account_id: data.target
                }
            })
        }).catch((err) => {
            throw err
        });
};

/**
 * @param {Object} data object with discord_id and steam_id 
 * @returns {Object} account Object{}
 */
function updateOrCreateAccount(data) {
    return Accounts.findOrCreate({
        where: {
            discord_id: data.discord_id // user account id
        }
    })
        .then((account) => {
            return account[0].update({
                steam_id: data.steam_id // user's manual input steamid
            })
        }).catch((err) => {
            throw err;
        });
};

/**
 * @param {Object} data object with discord_id and steam_id 
 * @returns {Object} account Object{}
 */
function findAccount(data) {
    return Accounts.findOne({
        where: {
            discord_id: data.discord_id // user account id
        }
    })
};

/**
 * 
 * @param {Object} data
 * @returns {Array} table[] 
 */
function addLRTable(data) {
    return LR_Tables.findOrCreate({
        where: {
            gid: data.gid,                   // Guild ID
            table_name: data.table_name, // UNIQUE: Prefix for {prefix}_base and etc modules
            server_name: data.server_name,   // Server Name
            server_ip: data.server_ip,       // Server IP (Full: IP:PORT)
            old_syntax: data.old_syntax       // Use old syntax (structure) for this table? boolean
        }
    })
};

/**
 * 
 * @param {Object} data
 * @returns {Number} 1 (true) if find and deleted (destroy), 0 (false) if didn't find any thing. 
 */
function delLRTable(data) {
    return LR_Tables.destroy({
        where: {
            gid: data.gid,                   // Guild ID
            table_name: data.table_name,    // UNIQUE: table_name
        }
    })
};

/**
 *
 *
 * @param {*} data
 * @returns
 */
function getLRServersList(data) {
    return DataTables.get_lr_tables(data);
};

/**
 *
 *
 * @param {*} data
 * @returns
 */
function findLRUsersStatsBySteamID(data) {
    const lr_bases = DataTables.getLRBaseTable(data);

    return lr_bases
        .then((tables) => {

            if (tables < 1) throw "Подключите таблицы с игроками";

            return Promise.all(tables.map(table => {
                return table.findOne({
                    where: {
                        steam: data.target
                    }
                })
            }))
        }).catch((err) => {
            throw err
        });
};

/**
 *
 *
 * @param {*} data
 * @returns
 */
function findLRUsersWeaponsStatsBySteamID(data) {
    const lr_bases = DataTables.getLR_WeaponsTables(data);

    return lr_bases
        .then((tables) => {

            if (tables < 1) throw "У вас установлен модуль _weapons с новой структурой (`steam, classname, kills`)?";

            return Promise.all(tables.map(table => {
                return table.findAll({
                    where: {
                        steam: data.target
                    }
                })
            }))
        }).catch((err) => {
            throw err
        });
};

function LR_getUserUnusualKillsStats(data) {
    const lr_unusual_kills = DataTables.getLR_UnusualKillsTables(data);

    return lr_unusual_kills
        .then(tables => {
            if (tables < 1) throw "У вас установлен модуль _unusualkills?";

            return Promise.all(tables.map(table => {
                return table.findAll({
                    where: {
                        SteamID: data.target
                    }
                })
            }))

        })
        .catch(err => {
            throw err;
        })
};


function LR_getUserHitsStats(data) {
    const lr_hits = DataTables.LR_getHitsTables(data);

    return lr_hits
        .then(tables => {
            if (tables < 1) throw "У вас установлен модуль _hits?";

            return Promise.all(tables.map(table => {
                return table.findAll({
                    where: {
                        SteamID: data.target
                    }
                })
            }))

        })
        .catch(err => {
            throw err;
        })
};

// todo: create categories.
// examples: LR, FPS, Accounts, SteamID etc
module.exports = {
    findOrCreateGuild,
    createDatabaseSetup,
    destroyDatabaseSetup,
    findUserByAccountID,
    findUserStatsByAccountID,
    findWeaponsStatsByAccountID,
    getFPSServersList,
    findFPSUnusualKillsStatsByAccountID,
    updateOrCreateAccount,
    findAccount,
    addLRTable,
    delLRTable,
    getLRServersList,
    findLRUsersStatsBySteamID,
    findLRUsersWeaponsStatsBySteamID,
    LR_getUserUnusualKillsStats,
    LR_getUserHitsStats
}