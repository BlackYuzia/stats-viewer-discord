const SteamID = require('steamid');

// todo: refactory two (or more) functions to one (with arguments)
// todo: or, create two functions. Parse and Converting function, and function with get target(s) for query

/**
 * return SteamID 64 or any String
 * @param {String} sid Steamid or any String
 */
function parseSteamID64(sid) {
    let steamid = '';
    try {
        steamid = new SteamID(sid);
        if (steamid.isValid()) return steamid.getSteamID64();

        return sid;
    } catch (err) {
        return sid
    }
};

/**
 * @param {String} sid Steamid or any String
 * @return AccountID or any String
 */
function parseAccountID(sid) {
    let steamid = '';
    try {
        steamid = new SteamID(sid);
        if (steamid.isValid()) return steamid.accountid;
        return sid;
    } catch (err) {
        return sid
    }
};

/**
 * @param {String} sid Steamid or any String
 * @return SteamID2 or any String
 */
function parseSteamID2(sid) {
    let steamid = '';
    try {
        steamid = new SteamID(sid);
        if (steamid.isValid()) return steamid.getSteam2RenderedID(1);

        return sid;
    } catch (err) {
        return sid
    }
};

module.exports = {
    parseSteamID64,
    parseSteamID2,
    parseAccountID
}
