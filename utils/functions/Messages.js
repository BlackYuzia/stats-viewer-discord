const { MessageEmbed } = require('discord.js');
const msg = {};
msg.cancel = "\nЧто бы прекратить попытку ввода, напишите: cancel.";

msg.colors = {};
/* 
Embed colors
DEFAULT: 0,
AQUA: 1752220,
GREEN: 3066993,
BLUE: 3447003,
PURPLE: 10181046,
GOLD: 15844367,
ORANGE: 15105570,
RED: 15158332,
GREY: 9807270,
DARKER_GREY: 8359053,
NAVY: 3426654,
DARK_AQUA: 1146986,
DARK_GREEN: 2067276,
DARK_BLUE: 2123412,
DARK_PURPLE: 7419530,
DARK_GOLD: 12745742,
DARK_ORANGE: 11027200,
DARK_RED: 10038562,
DARK_GREY: 9936031,
LIGHT_GREY: 12370112,
DARK_NAVY: 2899536 
*/
msg.colors.err = 15158332;
msg.colors.command = 3447003;
msg.colors.send = 3066993;
msg.colors.help = 7506394;
msg.colors.gold = 15844367;
msg.colors.aqua = 1752220;

msg.embed = {};

// allow you send any embed message to discord channel.
msg.embed.send = function (message, title, body, color, footer, attachmentsObj, fieldsObj) {
    const embed = new MessageEmbed()
    if (title) embed.setTitle(title);
    if (body) embed.setDescription(body)
    if (color) embed.setColor(color);
    if (footer) embed.setFooter(footer);
    if (attachmentsObj) {
        /* 
        attachmentsObj
        .attachment - file to attachment
        .image - file (from attachments) to add to bottom-left(mid) position
        .thumbnail - file (from attachments) to add to top-right position
        .
        */
        // todo: add support multiples attachments, images etc
        if (attachmentsObj.attachment) embed.attachFiles(attachmentsObj.attachment);
        if (attachmentsObj.image) embed.setImage(attachmentsObj.image);
        if (attachmentsObj.thumbnail) embed.setThumbnail(attachmentsObj.thumbnail);
    };
    if (fieldsObj) {
        /* 
        fieldsObj
        .name - array with names
        .body - array with bodyes
        .inline - inline mode enabled? (boolean)
        */
        fieldsObj.name.forEach((e, i) => {
            embed.addField(e, fieldsObj.body[i], fieldsObj.inline);
        });
    }
    message.channel.send(embed);
}

// created special for discord-akairo commands
msg.embed.reply = function (title, body, file) {
    const embed = new MessageEmbed()
        .setFooter(msg.cancel)
        .setColor(msg.colors.command);

    if (title) embed.setTitle(title);
    if (body) embed.setDescription(body)
    if (file) embed.attachFile(file);
    return { embed };
};

msg.fn = {};

/** 
 * @param {Array} name
 * @param {Array} body
 * @return {Array} Completed fields
*/
msg.fn.createFields = function (name, body, inline) {

    if (name.length < 1 || body.length !== name.length) return 0;

    const fields = {};
    fields.name = [];
    fields.body = [];
    if (inline) fields.inline = true;

    name.forEach((name, i) => {
        fields.name.push(name);
        fields.body.push(body[i]);
    })

    return fields;
};

module.exports = msg;