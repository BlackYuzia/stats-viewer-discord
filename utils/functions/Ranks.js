const { DataTables, config } = require('../../index');
const { MessageAttachment } = require('discord.js');
const fs = require('fs');

// Default paths to folders with ranks
const FPS_path = config.Ranks.FPS;
const LR_path = config.Ranks.LR;
const FPS_Ranks = getRanksList(FPS_path);
const LR_Ranks = getRanksList(LR_path);
// todo: add FPS and LR ranks support
let RanksList = fs.readdirSync(config.Ranks.FPS);
RanksList = RanksList.filter(file => file.endsWith(".png"));

// Remove all spaces from ranks files 
FPS_Ranks.forEach(file => {
    fs.renameSync(config.Ranks.FPS + file, config.Ranks.FPS + file.replace(/\s/g, ''))
})
LR_Ranks.forEach(file => {
    fs.renameSync(config.Ranks.LR + file, config.Ranks.LR + file.replace(/\s/g, ''))
})

function findRankByIDs(data) {
    const Table = DataTables.getFpsRanksTable(data);

    return Table.then(table => {
        return table.findAll({
            where: {
                rank_id: data.settingID   // settings_server_rank_id
            }
        })
            .then(ranks => {
                return ranks[data.rankElement] ? ranks[data.rankElement].rank_name : null;
            }).catch((err) => {
                throw err
            });
    })
};

/* Refactory */


/**
 *
 *
 * @param {*} path
 * @returns
 */
function getRanksList(path) {
    const files = fs.readdirSync(path);
    return files.filter(file => file.endsWith(".png") || file.endsWith(".jpeg"));
};

function FPS_getRank(data) {
    const Rank = findRankByIDs(data);

    return Rank
        .then((rank) => {
            // Не костыль - а object...
            const rank_name = (rank ? rank : "skillgroup_none").replace(/\s/g, '') + ".png";
            const attachmentObj = getAttachment(FPS_path, rank_name);
            return attachmentObj;
        }).catch((err) => {
            throw err;
        });
};



/**
 *
 *
 * @param {*} rank_id
 * @returns
 */
function LR_getRank(rank_id) {
    // todo: add support .png and .jpeg files
    const rank_name = `skillgroup${parseInt(rank_id) ? rank_id : "_none"}.png`;
    const attachmentObj = getAttachment(LR_path, rank_name);
    return attachmentObj;
};


/**
 *
 *
 * @param {*} path
 * @param {*} rank_name
 * @returns
 */
function getAttachment(path, rank_name) {
    if (!fs.existsSync(path + rank_name)) return {
        attachment: new MessageAttachment(path + "skillgroup_none.png"),
        thumbnail: "attachment://skillgroup_none.png"
    };
    const attachment = new MessageAttachment(path + rank_name);
    const thumbnail = `attachment://${rank_name}`;
    return { attachment, thumbnail };
};

module.exports = {
    findRankByIDs,
    LR_getRank,
    FPS_getRank
}