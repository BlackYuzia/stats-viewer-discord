const { Databases, LR_Tables, config } = require('../../index.js');
const { AES, enc } = require('crypto-js');
const Sequelize = require('sequelize');

// todo: add full description for functions (@param(s), @return(s) etc)
/**
 * @param {String} data guild id and db type [FPS or LR]
 * @return database
 */
function getDatabase(data) {
    console.log("[Debug:getDatabases(data)]");
    console.table(data);

    return Databases.findOne({
        where:
        {
            gid: data.gid,
            type: data.type
        }
    })
        .then((database) => {
            let error = 0;

            if (!database) throw `Настройте базу [${data.type}] для этого канала`

            // ? Debug
            const keys = Object.keys(database.dataValues);
            keys.forEach(key => {
                console.log("[Debug:getDatabases()]", key, database[key]);
                if (!database[key]) error++;
            });

            if (error) throw "Один из параметров null"

            // Decrypt pass
            const decrypt = AES.decrypt(database.pass, config.crypto).toString(enc.Utf8);

            // Setup connect to DB
            const DB = new Sequelize(database.database, database.user, decrypt, {
                host: database.host,
                port: database.port,
                dialect: database.dialect,
                logging: false,
                operatorsAliases: false,
            });

            DB
                .authenticate()
                .catch(err => {
                    throw err
                });

            return DB;
        }).catch((err) => {
            throw err
        });
};

/**
 * @param {Object} Guild findOrCreateGuild() => Guild data
 * @return attached database to this guild
 */
function getDatabaseDataFromGuild(Guild) {
    return Guild.getDatabases();
}

/**
 * @param {String} data 
 * @return Promise with fps_players table from connected database
 */
function getFpsPlayersTable(data) {
    // Find connect data by data (lol)
    const DB = getDatabase(data);

    return DB
        .then((db) => {
            // Return table fps_players
            return require("../../models/FPS/fps_players")(db, Sequelize);
        }).catch(err => {
            console.error("[DEBUG:getFpsPlayersTable:catch(err)]", err);
            throw err;
        });
}

/**
 * @param {String} data 
 * @return Promise with fps_servers table from connected database
 */
function getFpsServersTable(data) {
    // Find connect data
    const DB = getDatabase(data);

    return DB
        .then((db) => {

            // Return table fps_servers
            return require("../../models/FPS/fps_servers.js")(db, Sequelize);
        }).catch(err => {
            console.error("[DEBUG:getFpsServersTable:catch(err)]", err);
            throw err;
        });
}

/**
 * @param {String} data 
 * @return Promise with fps_servers_stats table from connected database
 */
function getFpsServersStatsTable(data) {
    // Find connect data
    const DB = getDatabase(data);

    return DB
        .then((db) => {
            // Return table fps_servers_stats
            return require("../../models/FPS/fps_servers_stats.js")(db, Sequelize);
        }).catch(err => {
            console.error("[DEBUG:getFpsServersStatsTable:catch(err)]", err);
            throw err;
        });
}

/**
 * @param {String} data 
 * @return Promise with fps_weapons_stats table from connected database
 */
function getFpsWeaponsStatsTable(data) {
    // Find connect data
    const DB = getDatabase(data);

    return DB
        .then((db) => {
            // Return table fps_weapons_stats
            return require("../../models/FPS/fps_weapons_stats.js")(db, Sequelize);
        }).catch(err => {
            console.error("[DEBUG:getFpsWeaponsStatsTable:catch(err)]", err);
            throw err;
        });
}

/**
 * @param {String} data 
 * @return Promise with fps_ranks table from connected database
 */
function getFpsRanksTable(data) {
    // Find connect data
    const DB = getDatabase(data);

    return DB
        .then((db) => {
            // Return table fps_ranks
            return require("../../models/FPS/fps_ranks.js")(db, Sequelize);
        }).catch(err => {
            console.error("[DEBUG:getFpsRanksTable:catch(err)]", err);
            throw err;
        });
}


/**
 *
 *
 * @param {*} data
 * @returns
 */
function getFpsUnusualKillsTable(data) {
    // Find connect data
    const DB = getDatabase(data);

    return DB
        .then((db) => {
            // Return table fps_ranks
            return require("../../models/FPS/Modules/fps_unusualkills.js")(db, Sequelize);
        }).catch(err => {
            console.error("[DEBUG:getFpsUnusualKillsTable:catch(err)]", err);
            throw err;
        });
}

/*
 
  __       ___________    ____  _______  __         .______          ___      .__   __.  __  ___      _______.
 |  |     |   ____\   \  /   / |   ____||  |        |   _  \        /   \     |  \ |  | |  |/  /     /       |
 |  |     |  |__   \   \/   /  |  |__   |  |        |  |_)  |      /  ^  \    |   \|  | |  '  /     |   (----`
 |  |     |   __|   \      /   |   __|  |  |        |      /      /  /_\  \   |  . `  | |    <       \   \    
 |  `----.|  |____   \    /    |  |____ |  `----.   |  |\  \----./  _____  \  |  |\   | |  .  \  .----)   |   
 |_______||_______|   \__/     |_______||_______|   | _| `._____/__/     \__\ |__| \__| |__|\__\ |_______/    
                                                                                                              
 
*/

/**
 * @param {Object} data
 * @return array with lr_tables(prefixes etc)[]
 */
function get_lr_tables(data) {
    return LR_Tables.findAll({
        where: {
            gid: data.gid // guild id
        }
    })
};

// todo: migration from table to tables
/**
 *
 *
 * @param {*} data
 * @returns
 */
function getLRBaseTable(data) {
    const DB = getDatabase(data);
    const lr_tables = get_lr_tables(data);

    return DB
        .then((db) => {
            return lr_tables
                .then((tables) => {
                    return Promise.all(tables.map(table => {
                        return require("../../models/LR/table_base.js")(db, Sequelize, table.table_name);

                    }));

                })
                .catch((err) => {
                    throw err;
                });
        }).catch((err) => {
            console.error("[DEBUG:getLRBaseTable:catch(err)]", err);
            throw err;
        });
}

/**
 *
 *
 * @param {*} data
 * @returns
 */
function getLR_WeaponsTables(data) {
    const DB = getDatabase(data);
    const lr_tables = get_lr_tables(data);

    return DB
        .then((db) => {
            return lr_tables
                .then((tables) => {
                    return Promise.all(tables.map(table => {
                        // if old syntax is true, use old database structure
                        // todo: maybe refactory "old_syntax" to something better (like: lr_tables => table_name => lr_modules => syntax: new, old etc)
                        if (table.old_syntax) {
                            return require("../../models/LR/Modules/table_weapons_old.js")(db, Sequelize, table.table_name);
                        }

                        return require("../../models/LR/Modules/table_weapons.js")(db, Sequelize, table.table_name);

                    }));

                })
                .catch((err) => {
                    throw err;
                });
        }).catch((err) => {
            console.error("[DEBUG:getLR_WeaponsTables:catch(err)]", err);
            throw err;
        });
}

/**
 *
 *
 * @param {*} data
 * @returns
 */
function getLR_UnusualKillsTables(data) {
    const DB = getDatabase(data);
    const lr_tables = get_lr_tables(data);

    return DB
        .then((db) => {
            return lr_tables
                .then((tables) => {
                    return Promise.all(tables.map(table => {
                        return require('../../models/LR/Modules/table_unusualkills.js')(db, Sequelize, table.table_name);
                    }));

                })
                .catch((err) => {
                    throw err;
                });
        }).catch((err) => {
            console.error("[DEBUG:getLR_UnusualKillsTables:catch(err)]", err);
            throw err;
        });
}

/**
 *
 *
 * @param {*} data
 * @returns
 */
function LR_getHitsTables(data) {
    const DB = getDatabase(data);
    const lr_tables = get_lr_tables(data);

    return DB
        .then((db) => {
            return lr_tables
                .then((tables) => {
                    return Promise.all(tables.map(table => {
                        return require('../../models/LR/Modules/table_hits.js')(db, Sequelize, table.table_name);
                    }));

                })
                .catch((err) => {
                    throw err;
                });
        }).catch((err) => {
            console.error("[DEBUG:LR_getHitsTables:catch(err)]", err);
            throw err;
        });
}


module.exports = {
    getDatabase,
    getDatabaseDataFromGuild,
    getFpsPlayersTable,
    getFpsRanksTable,
    getFpsUnusualKillsTable,
    getFpsServersStatsTable,
    getFpsServersTable,
    getFpsWeaponsStatsTable,
    get_lr_tables,
    getLRBaseTable,
    getLR_WeaponsTables,
    getLR_UnusualKillsTables,
    LR_getHitsTables
}