const Sequelize = require('sequelize');
const { db } = require('../configs/config.json');

// Database where this bot was save all our data
const database = new Sequelize(db.name, db.user, db.pass, {
    host: db.host,
    port: db.port,
    dialect: db.dialect,
    logging: false,
    operatorsAliases: false,
});

database
    .authenticate()
    .catch((err) => {
        console.log("[ОШИБКА] Не могу подключиться к моей (client) базе данных", err);
    });

// Guilds, table with saved guilds id (data)
const Guilds = database.define('Guilds', {
    id: {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
    }
}, { timestamps: false });


// Databases, table with saved databases data (host, user, pass etc)
const Databases = database.define('Databases', {
    gid: { // guild id
        type: Sequelize.STRING(64),
        primaryKey: true,
        allowNull: false,
    },
    type: { // ["FPS" || "LR"]
        type: Sequelize.STRING(64), // Н-На Запас
        primaryKey: true,
        allowNull: false
    },
    host: {
        type: Sequelize.STRING(64),
        allowNull: true,
        defaultValue: null
    },
    database: {
        type: Sequelize.STRING(64),
        allowNull: true,
        defaultValue: null
    },
    user: {
        type: Sequelize.STRING(64),
        allowNull: true,
        defaultValue: null
    },
    pass: {
        type: Sequelize.STRING(1024),
        allowNull: true,
        defaultValue: null
    },
    dialect: {
        type: Sequelize.STRING(64),
        defaultValue: "mysql"
    },
    port: {
        type: Sequelize.STRING(64),
        defaultValue: 3306
    }
}, { timestamps: false });

// LR_Tables, table with saved tables prefixes data (ONLY for LR)
const LR_Tables = database.define('lr_tables', {
    gid: { // guild id
        type: Sequelize.STRING(64),
        // unique: false,
        primaryKey: true,
        allowNull: false,
    },
    table_name: {
        type: Sequelize.STRING(64),
        // unique: true,
        primaryKey: true,
        allowNull: false
    },
    server_name: {
        type: Sequelize.STRING(64),
        allowNull: false
    },
    server_ip: {
        type: Sequelize.STRING(64),
        allowNull: false
    },
    old_syntax: { // true or false
        type: Sequelize.BOOLEAN,
        allowNull: true
    }
}, { timestamps: false });


// Accounts, table with saved discord accounts with attached steamid
const Accounts = database.define('Accounts', {
    discord_id: { // discord account id
        type: Sequelize.STRING(64),
        unique: true
    },
    steam_id: { // attached steamid to discord_id
        type: Sequelize.STRING(64)
    }
}, { timestamps: false });

Guilds.hasMany(Databases, { foreignKey: "gid" });
Guilds.hasMany(LR_Tables, { foreignKey: "gid" });
Databases.belongsTo(Guilds, { foreignKey: "gid" });
LR_Tables.belongsTo(Guilds, { foreignKey: "gid" });

// Sync all tables
database
    .sync()
    .catch((err) => {
        console.log(err);
    });

module.exports = {
    Guilds,
    Databases,
    Accounts,
    LR_Tables
};
